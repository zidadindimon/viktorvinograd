function navigationForPage(tab){
    var tabs = document.getElementsByClassName('navigation-tab');
    for( var item = 0; item < tabs.length; item ++){
        $(tabs[item]).removeClass('active');
    }
    $(tab).addClass('active');
}

$(document).ready(function(){
    $("#navigation").on("click","a", function (event) {
        event.preventDefault();
        var id  = $(this).attr('href');
        avimationScrolForPage(id);
        navigationForPage($(this).parent());
    });

});

window.onload = function(){
    console.log(window.location.hash);
    if(window.location.hash.length > 2){
        avimationScrolForPage(''+window.location.hash+'');
        navigationForPage(''+window.location.hash+'-tab');
    }
};

function avimationScrolForPage(id){
    var top = $(id).offset().top-65;
    $('body,html').animate({scrollTop: top}, 1500);
}

var slideIndex = 0;
carousel();

function carousel() {
    var i;
    var x = document.getElementsByClassName("mySlides");
    for (i = 0; i < x.length; i++) {
        $(x[i]).css('display',"none");
    }
    slideIndex++;
    if (slideIndex > x.length) {slideIndex = 1}
    $( x[slideIndex-1]).css('display',"block");
    setTimeout(carousel, 2000); // Change image every 2 seconds
}

$(function () {
    initSetings();
    // Initialize photo gallery
    var photoCarousel = $(".photo-carousel");
    var photoItems = photoCarousel.find(".item-img").length;
    if (photoItems) {
        photoCarousel.owlCarousel({
            navSpeed:   500,
            navText:    false,
            items:      1,
            nav:        true,
            loop:       true,
            center:     true,
            autoplay:   false,
            autoplayTimeout:2000,
            autoplayHoverPause:false
        });
    }

});

$(window).resize(function () {
   initSetings();
});

initSetings();

function initSetings(){
    console.log($(window).height());
    var desktopHeight = $(window).height();
    $(' body').css('min-height', desktopHeight+'px');
    $('.carousel-wrapper').css('height', desktopHeight+5+'px');
    $('.item-img').css('height', desktopHeight+'px');
    $('.next').css('margin-top', desktopHeight-60+'px');
    $('#price_list').css('margin-top', desktopHeight+'px');
    $('#contact').css('min-height', desktopHeight-120+'px');


}

function priceListClick(){
    console.log('click-price ');
    var top = $('#price_list').offset().top-75;
    $('body,html').animate({scrollTop: top}, 1500);
}

window.onscroll = function() {
    var scrolled = window.pageYOffset || document.documentElement.scrollTop;
    console.log(scrolled + 'px');
};

function FeedbackForm(form) {

    $.post(
        "php/mail.php",
        {
            name: $('#name').val(),
            phone: $('#phone').val(),
            email: $('#email').val(),
            subject: $('#subject').val(),
            message: $('#message').val(),
        },
        function(data){

        }
    );

}